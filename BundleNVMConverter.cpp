#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>
#include <time.h>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include <functional>
#include <numeric>
#include <sstream>
#include "util.h"
using namespace std;



int main(int argc, char* argv[])
{
	char* inputfile = argv[1];
	char* outputfile = argv[2];
	char* imagelist = argv[3];
	
	vector<CameraT> camera_data;
	vector<Point3D> point_data;
	vector<Point2D> measurements;
	vector<int> ptidx;
	vector<int> camidx;
	vector<string> names; 
	vector<int> ptc;
	
	LoadModelFile(inputfile,imagelist,camera_data,point_data,measurements,ptidx,camidx,names,ptc);
	SaveModelFile(outputfile,camera_data,point_data,measurements,ptidx,camidx,names,ptc);
	
	
	
	return 1;
}
